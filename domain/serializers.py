from rest_framework import serializers
from models import Website,Contact_information


class ContactSerializer(serializers.ModelSerializer):
	class Meta:
		model = Contact_information
		fields = ('Contact_Name','Email','Alerts_Enabled')		

class WebsiteSerializer(serializers.ModelSerializer):
	#Contacts = ContactSerializer()
	class Meta:
		model = Website
		field = ('domain_name','domain_provider','expiry_date','Hosting_server','hosting_provider','auto_renewal','https_Enabled','comments')

