from __future__ import unicode_literals

from django.db import models

from django.conf import settings
from django.core.urlresolvers import reverse
from auditable.models import Auditable
from simple_history.models import HistoricalRecords

class Contact_information(Auditable):
    def __str__(self):
        return self.Contact_Name
    Contact_Name = models.CharField(max_length=100)
    Email = models.EmailField(max_length=100)
    Alerts_Enabled = models.BooleanField(default=False)

class CMS_info(models.Model):
    CMS = models.CharField(max_length=100,blank=True,null=True)
    def __str__(self):
        return self.CMS

class OS_Platform(models.Model):
    OS = models.CharField(max_length=100,blank=True,null=True)
    def __str__(self):
        return self.OS

class Department(models.Model):
    Department = models.CharField(max_length=100,blank=True,null=True)
    Contact_Person = models.CharField(max_length=100,blank=True,null=True)
    Contact_Person_Email = models.EmailField(max_length=100,blank=True,null=True)
    Contact_Number = models.CharField(max_length=100,blank=True,null=True)
    def __str__(self):
        return self.Department
    
HT_choices = (
              ('Shared','Shared'),
              ('Dedicated','Dedicated')
              )
   
class Website(Auditable):
    def __str__(self):
        return self.domain_name
    domain_name = models.CharField(max_length=100,unique=True)
    domain_provider = models.CharField(max_length=100)
    expiry_date = models.DateField(blank=True,null=True)
    Hosting_server = models.CharField(max_length=100,blank=True,null=True)
    Hosting_Type = models.CharField(max_length=20,choices=HT_choices,blank=True,null=True)
    hosting_provider = models.CharField(max_length=100,blank=True,null=True)
    auto_renewal = models.BooleanField(default=False)
    https_Enabled = models.BooleanField(default=False)
    Contacts = models.ManyToManyField(Contact_information,related_name='Websites',blank=True,)    
    CMS = models.ForeignKey(CMS_info)
    OS_Platform = models.ForeignKey(OS_Platform)
    comments = models.TextField(blank=True,null=True)
    history = HistoricalRecords()
    Developer = models.CharField(max_length=100,blank=True,null=True)
    Developer_Email = models.EmailField(max_length=100,blank=True,null=True)
    Developer_Contact_number = models.CharField(max_length=100,blank=True,null=True)
    Business_Department = models.ForeignKey(Department,related_name='department')
    CMS_Username = models.CharField(max_length=100,blank=True,null=True)
    CMS_Password = models.CharField(max_length=100,blank=True,null=True)
    Hosting_or_Cpanel_Username = models.CharField(max_length=100,blank=True,null=True)
    Hosting_or_Cpanel_Password = models.CharField(max_length=100,blank=True,null=True)

    class Meta:
        ordering = ["-domain_name"]
    
    def get_absolute_url(self):
        return reverse("site_list")

class DNS_Info(Auditable):
    def __str__(self):
        return self.domain_name.domain_name
    domain_name = models.ForeignKey(Website,on_delete=models.CASCADE,related_name='%(class)s_DNS_Info')
    A_Record = models.CharField(max_length=100,blank=True,null=True)
    CName_Record = models.CharField(max_length=100,blank=True,null=True)
    MX_Record = models.CharField(max_length=100,blank=True,null=True)
    history = HistoricalRecords()


   
class SSL_Information(models.Model):
    def __str__(self):
        return self.domain_name.domain_name
    domain_name = models.ForeignKey(Website,on_delete=models.CASCADE,related_name='SSL_Info')
    SSL_Presence = models.BooleanField(default=False)
    SSL_Expiry = models.DateField(blank=True,null=True)
    SSL_Provider = models.CharField(max_length=100,blank=True,null=True)
