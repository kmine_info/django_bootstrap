from menu import Menu, MenuItem
from django.core.urlresolvers import reverse


Menu.add_item("Home", MenuItem("Home Page", reverse("domain:Landing")))

Menu.add_item("Websites", MenuItem("List Websites",  reverse("domain:site_list")))

Menu.add_item("Websites", MenuItem("Add Website", reverse("domain:create_site")))


