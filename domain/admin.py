from django.contrib import admin

# Register your models here.
from domain.models import Website,DNS_Info,Contact_information,Department,CMS_info,OS_Platform,SSL_Information


class ContactAdmin(admin.ModelAdmin):
    exclude = ('created_by','modified_by')
    list_display = ('Contact_Name', 'Email', 'Alerts_Enabled')
    
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('Department', 'Contact_Person', 'Contact_Person_Email','Contact_Number')
    
class DNS_InfoAdmin(admin.ModelAdmin):
    list_display = ('domain_name', 'A_Record', 'CName_Record','MX_Record')

class WebsiteAdmin(admin.ModelAdmin):
    exclude = ('created_by','modified_by')
    list_display = ('domain_name', 'expiry_date', 'Hosting_server','Hosting_Type','auto_renewal','Business_Department')

admin.site.register(Website,WebsiteAdmin)
admin.site.register(Contact_information,ContactAdmin)
admin.site.register(Department,DepartmentAdmin)
admin.site.register(DNS_Info,DNS_InfoAdmin)
admin.site.register(OS_Platform)
admin.site.register(CMS_info)
admin.site.register(SSL_Information)




