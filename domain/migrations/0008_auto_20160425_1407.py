# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('domain', '0007_historicaltechnology_info_technology_info'),
    ]

    operations = [
        migrations.CreateModel(
            name='CMS_info',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('CMS', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Department', models.CharField(max_length=100, null=True, blank=True)),
                ('Contact_Person', models.CharField(max_length=100, null=True, blank=True)),
                ('Contact_Person_Email', models.EmailField(max_length=100, null=True, blank=True)),
                ('Contact_Number', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='OS_Platform',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('OS', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='historicaltechnology_info',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='historicaltechnology_info',
            name='domain_name',
        ),
        migrations.RemoveField(
            model_name='historicaltechnology_info',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicaltechnology_info',
            name='modified_by',
        ),
        migrations.RemoveField(
            model_name='technology_info',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='technology_info',
            name='domain_name',
        ),
        migrations.RemoveField(
            model_name='technology_info',
            name='modified_by',
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='CMS_Password',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='CMS_Username',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='Developer',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='Developer_Contact_number',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='Developer_Email',
            field=models.EmailField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='website',
            name='CMS_Password',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='website',
            name='CMS_Username',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='website',
            name='Developer',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='website',
            name='Developer_Contact_number',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='website',
            name='Developer_Email',
            field=models.EmailField(max_length=100, null=True, blank=True),
        ),
        migrations.DeleteModel(
            name='HistoricalTechnology_Info',
        ),
        migrations.DeleteModel(
            name='Technology_Info',
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='Business_Department',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='domain.Department', null=True),
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='CMS',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='domain.CMS_info', null=True),
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='OS_Platform',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='domain.OS_Platform', null=True),
        ),
        migrations.AddField(
            model_name='website',
            name='Business_Department',
            field=models.ForeignKey(related_name='department', default=1, to='domain.Department'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='website',
            name='CMS',
            field=models.ForeignKey(default=1, to='domain.CMS_info'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='website',
            name='OS_Platform',
            field=models.ForeignKey(default=1, to='domain.OS_Platform'),
            preserve_default=False,
        ),
    ]
