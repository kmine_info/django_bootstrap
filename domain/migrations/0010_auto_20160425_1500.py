# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domain', '0009_auto_20160425_1458'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalwebsite',
            name='Hosting_or_Cpanel_Password',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='historicalwebsite',
            name='Hosting_or_Cpanel_Username',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='website',
            name='Hosting_or_Cpanel_Password',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='website',
            name='Hosting_or_Cpanel_Username',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
