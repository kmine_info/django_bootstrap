# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domain', '0008_auto_20160425_1407'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalwebsite',
            name='Hosting_Type',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[('Shared', 'Shared'), ('Dedicated', 'Dedicated')]),
        ),
        migrations.AddField(
            model_name='website',
            name='Hosting_Type',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[('Shared', 'Shared'), ('Dedicated', 'Dedicated')]),
        ),
    ]
