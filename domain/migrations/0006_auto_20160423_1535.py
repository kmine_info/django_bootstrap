# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domain', '0005_auto_20160422_2153'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='Contacts',
            field=models.ManyToManyField(related_name='Websites', to='domain.Contact_information', blank=True),
        ),
    ]
