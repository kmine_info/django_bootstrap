# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('domain', '0006_auto_20160423_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalTechnology_Info',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('created_on', models.DateTimeField(editable=False, blank=True)),
                ('modified_on', models.DateTimeField(editable=False, blank=True)),
                ('CMS', models.CharField(max_length=100, null=True, blank=True)),
                ('OS_Platform', models.CharField(max_length=100, null=True, blank=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('created_by', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('domain_name', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='domain.Website', null=True)),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
                ('modified_by', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical technology_ info',
            },
        ),
        migrations.CreateModel(
            name='Technology_Info',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('CMS', models.CharField(max_length=100, null=True, blank=True)),
                ('OS_Platform', models.CharField(max_length=100, null=True, blank=True)),
                ('created_by', models.ForeignKey(related_name='technology_info_Created_By', to=settings.AUTH_USER_MODEL)),
                ('domain_name', models.ForeignKey(related_name='Tech_Info', to='domain.Website')),
                ('modified_by', models.ForeignKey(related_name='technology_info_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
