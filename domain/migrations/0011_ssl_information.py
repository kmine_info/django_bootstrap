# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domain', '0010_auto_20160425_1500'),
    ]

    operations = [
        migrations.CreateModel(
            name='SSL_Information',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('SSL_Presence', models.BooleanField(default=False)),
                ('SSL_Expiry', models.DateField(null=True, blank=True)),
                ('SSL_Provider', models.CharField(max_length=100, null=True, blank=True)),
                ('domain_name', models.ForeignKey(related_name='SSL_Info', to='domain.Website')),
            ],
        ),
    ]
