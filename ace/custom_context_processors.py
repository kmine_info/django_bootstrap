from ace.models import Company_profile


def comp_profile(request):
    try:
        comp = Company_profile.objects.get(id=1)
        return {'comp_profile_name': comp.name,
                'comp_profile_icon' : comp.icon,
                'comp_company_logo' : comp.company_logo.url,
                'comp_favicon' : comp.favicon.url,
                'comp_company_logo_height':comp.logo_height,
                'comp_company_logo_width':comp.logo_width,
            }

    except:
        return {
                'comp_profile_name': 'foo',
                'comp_profile_icon' : 'leaf',
                
                }