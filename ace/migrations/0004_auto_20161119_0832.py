# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ace', '0003_auto_20161119_0806'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company_profile',
            name='company_logo',
            field=models.ImageField(default=b'logo/Kmine_Log.png', height_field=b'50', width_field=b'50', upload_to=b'logo'),
        ),
    ]
