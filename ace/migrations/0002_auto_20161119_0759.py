# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ace', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='company_profile',
            name='company_logo',
            field=models.ImageField(default=b'logo//Kmine_Log.png', upload_to=b'logo'),
        ),
        migrations.AddField(
            model_name='company_profile',
            name='favicon',
            field=models.ImageField(default=b'favicons/favicon.ico', upload_to=b'favicons'),
        ),
        migrations.AddField(
            model_name='company_profile',
            name='index_title',
            field=models.CharField(default=b'Tools Administration', max_length=50),
        ),
        migrations.AddField(
            model_name='company_profile',
            name='site_header',
            field=models.CharField(default=b'Kmine Administration Console', max_length=50),
        ),
        migrations.AddField(
            model_name='company_profile',
            name='site_title',
            field=models.CharField(default=b'Kmine Admin Console', max_length=50),
        ),
    ]
