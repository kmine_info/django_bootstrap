# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ace', '0002_auto_20161119_0759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company_profile',
            name='company_logo',
            field=models.ImageField(default=b'logo/Kmine_Log.png', upload_to=b'logo'),
        ),
    ]
