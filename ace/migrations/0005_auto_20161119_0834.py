# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ace', '0004_auto_20161119_0832'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company_profile',
            name='company_logo',
            field=models.ImageField(default=b'logo/Kmine_Log.png', height_field=50, width_field=50, upload_to=b'logo'),
        ),
    ]
