# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ace', '0006_auto_20161119_0837'),
    ]

    operations = [
        migrations.AddField(
            model_name='company_profile',
            name='logo_height',
            field=models.PositiveIntegerField(default=50),
        ),
        migrations.AddField(
            model_name='company_profile',
            name='logo_width',
            field=models.PositiveIntegerField(default=50),
        ),
    ]
