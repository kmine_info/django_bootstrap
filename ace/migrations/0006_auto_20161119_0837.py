# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ace', '0005_auto_20161119_0834'),
    ]

    operations = [
        migrations.AddField(
            model_name='company_profile',
            name='url_height',
            field=models.PositiveIntegerField(default=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company_profile',
            name='url_width',
            field=models.PositiveIntegerField(default=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='company_profile',
            name='company_logo',
            field=models.ImageField(default=b'logo/Kmine_Log.png', height_field=b'url_height', width_field=b'url_width', upload_to=b'logo'),
        ),
    ]
