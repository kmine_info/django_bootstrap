from django.db import models

# Create your models here.
nav_choices = (
               ('left','left'),
               ('top' , 'top'),
               )

class Company_profile(models.Model):
    
    name = models.CharField(max_length=50,default='Kmine app cloud')
    icon = models.CharField(max_length=50,default='leaf')
    site_header = models.CharField(max_length=50,default='Kmine Administration Console')
    site_title = models.CharField(max_length=50,default='Kmine Admin Console')
    index_title = models.CharField(max_length=50,default='Tools Administration')
    logo_height = models.PositiveIntegerField(default=50)
    logo_width = models.PositiveIntegerField(default=50)
    url_height=models.PositiveIntegerField()
    url_width=models.PositiveIntegerField()
    company_logo = models.ImageField(upload_to='logo',default = 'logo/Kmine_Log.png',width_field='url_width', height_field='url_height')
    favicon = models.ImageField(upload_to='favicons',default = 'favicons/favicon.ico')
    def __str__(self):
        return self.name

    
    