from __future__ import unicode_literals

from django.apps import AppConfig


class MyMenusConfig(AppConfig):
    name = 'my_menus'
